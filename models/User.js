const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is Required!"]
	},
	lastName: {
		type: String,
		required: [true, "Last name is Required!"]
	},
	email: {
		type: String,
		required: [true, "Email address is Required!"]
	},
	password: {
		type: String,
		required: [true, "Password is Required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is Required!"]
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "CourseID is Required!"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);